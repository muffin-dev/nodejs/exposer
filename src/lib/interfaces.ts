import { TExposerOperation } from './types';

/**
 * @interface IExposerOperation Represents an operation that set informations about an exposed class or property.
 */
export interface IExposerOperation {
  /**
   * @property Defines the type of the operation to apply.
   */
  type: TExposerOperation;

  /**
   * @property Defines the name of the target property. This is set only if the operation applies to a class.
   */
  propertyName?: string;

  /**
   * @property The optional target groups to set to the Exposer informations. If there's no group to set, this value should be an empty
   * array.
   */
  groups: string[];
}

/**
 * @interface IExposedClassData Represents the informations about a class that has exposed properties.
 */
export interface IExposedClassData {
  /**
   * @property If true, the entire class is exposed. Else, only the defined properties are exposed.
   */
  exposed: boolean;

  /**
   * @property Contains all the exposed properties
   */
  properties: Map<string, IExposedPropertyData>;

  /**
   * @property Used only when the @Expose() decorator is used on a class.
   */
  groups: Set<string>;
}

/**
 * @interface IExposedPropertyData Represents the informations about an exposed property.
 */
export interface IExposedPropertyData {
  /**
   * @property The names of the groups for which this property is exposed.
   */
  groups: Set<string>;

  /**
   * @property If true, avoids this property to be exposed. Considered only if the owning class is exposed.
   */
  excluded: boolean;
}
