import { IExposedClassData, IExposerOperation } from './interfaces';
import { TExposerOperation, TConstructor } from './types';
import { asArray } from '@muffin-dev/js-helpers';

export class Exposer {

  //#region Properties

  /**
   * @property The unresolved operations to apply for a referenced class.
   */
  private _unresolvedOperations = new Map<string, IExposerOperation[]>();

  /**
   * @property Contains the resolved data about exposable classes and properties. You should never use it directly and use _getClassData(),
   * which will resolved all unresolved operations that a class can have.
   */
  private _exposerData = new Map<string, IExposedClassData>();

  //#endregion

  //#region Public API

  /**
   * Makes a new object that contains only the exposed properties.
   * @template TObject The type of the input object.
   * @param instance The instance of a class you want to expose.
   * @param groups If given, only the properties exposed for the named groups will be set on the output object.
   * @returns Returns a new object with the exposed properties, or null if the input object is null.
   */
  public expose<TObject = Record<string, unknown>>(instance: TObject, groups?: string | string[]): Partial<TObject>;

  /**
   * Makes a new object that contains only the exposed properties.
   * @template TObject The type of the input object.
   * @param className The name of the class of the object to expose.
   * @param obj The object you want to expose.
   * @param groups If given, only the properties exposed for the named groups will be set on the output object.
   * @returns Returns a new object with the exposed properties, or null if the input object is null.
   */
  public expose<TObject = Record<string, unknown>>(
    className: string,
    obj: TObject | Record<string, unknown>,
    groups?: string | string[]
  ): Partial<TObject>;

  /**
   * Makes a new object that contains only the exposed properties.
   * @template TObject The type of the input object.
   * @param objectClass The class or (class constructor method) of the object to expose.
   * @param obj The object to expose. Can be either an instance of the target class, or a simple JavaScript object.
   * @param groups If given, only the properties exposed for the named groups will be set on the output object.
   * @returns Returns a new object with the exposed properties, or null if the input object is null.
   */
  public expose<TObject = Record<string, unknown>>(
    objectClass: TConstructor<TObject>,
    obj: TObject | Record<string, unknown>,
    groups?: string | string[]
  ): Partial<TObject>;

  /**
   * Makes a new object that contains only the exposed properties.
   * @template TObject The type of the input object.
   * @param objectClass An instance of the class you want to expose.
   * @param obj The object to expose. Can be either an instance of the target class, or a simple JavaScript object.
   * @param groups Used only if the first parameter is a class constructor or a class name. If given, only the properties exposed for the
   * named groups will be set on the output object.
   * @returns Returns a new object with the exposed properties, or null if the input object is null.
   * @throws {TypeError} Thrown if the "objectClassOrObj" parameter is neither a string, a constructor method or an instance of a class.
   */
  public expose<TObject = Record<string, unknown>>(
    objectClass: TObject,
    obj: TObject | Record<string, unknown>,
    groups?: string | string[]
  ): Partial<TObject>;

  /**
   * Makes a new object that contains only the exposed properties.
   * @template TObject The type of the input object.
   * @param objectClassOrObj An instance of a class, a class constructor method or the class name of the object to expose.
   * @param objOrGroups If the previous parameter is an object, this should contain the group(s) you want to target. In that case, only the
   * properties exposed for the named groups will be set on the output object. If the previous parameter is a class constructor or a class
   * name, this should contain the object you want to expose (even a instance of the named class, or a "regular" object).
   * @param groups Used only if the first parameter is a class constructor or a class name. If given, only the properties exposed for the
   * named groups will be set on the output object.
   * @returns Returns a new object with the exposed properties, or null if the input object is null.
   * @throws {TypeError} Thrown if the "objectClassOrObj" parameter is neither a string, a constructor method or an instance of a class.
   */
  public expose<TObject = Record<string, unknown>>(
    objectClassOrObj: TConstructor<TObject> | string | TObject,
    objOrGroups?: TObject | Record<string, unknown> | string | string[],
    groups?: string | string[]
  ): Partial<TObject> {
    // If the objectClass argument is a class constructor
    if (typeof objectClassOrObj === 'function') {
      // obj should be the instance, so a not-null TObject
      // groups should be the groups, so a string or array of strings
      return this._generateExposedObject(objectClassOrObj.name, objOrGroups as TObject, groups);
    }

    // If the objectClass argument is an instance of a class
    else if (typeof objectClassOrObj === 'object') {
      // obj should be the groups, so a string or an array of strings
      return this._generateExposedObject(objectClassOrObj.constructor.name, objectClassOrObj, objOrGroups as string | string[]);
    }

    // If the objectClass argument is a string (which should be a class name)
    else if (typeof objectClassOrObj === 'string') {
      // obj should be an instance of a class or a custom object
      // groups should be the groups, so a string or array of strings
      return this._generateExposedObject(objectClassOrObj, objOrGroups as TObject, groups);
    }

    else {
      throw new TypeError('Invalid argument: The first parameter of expose() must be a class, a class name or an instance of a class');
    }
  }

  /**
   * Marks a class or a property "exposed".
   * @param objectClass A constructor method, an instance or the name of the class on which the operation will be performed.
   * @param propertyName The name of the property on which to apply the operation. Null if the operation applies to a class.
   * @param groups The eventual group(s) that the class or the property should be exposed for.
   * @param fromDecorator If true, adds the operation at the top of the list, as decorator operations are resolved backwards. Otherwise,
   * adds the operation after othe existing ones.
   * @throws {TypeError} Thrown if the given "objectClass" argument is not valid.
   */
  public markExposed(
    objectClass: TConstructor | string | Record <string, unknown>,
    propertyName: string = null,
    groups: string | string[] = null,
    fromDecorator = false
  ): void {
    this._addOperation(objectClass, propertyName ? 'exposeProperty' : 'exposeClass', propertyName, groups, fromDecorator);
  }

  /**
   * Marks a property "exposed".
   * @param objectClass A constructor method, an instance or the name of the class on which the operation will be performed.
   * @param propertyName The name of the property on which to apply the operation.
   * @param fromDecorator If true, adds the operation at the top of the list, as decorator operations are resolved backwards. Otherwise,
   * adds the operation after othe existing ones.
   * @throws {TypeError} Thrown if the given "objectClass" argument is not valid.
   */
  public markExcluded(
    objectClass: TConstructor | string | Record <string, unknown>,
    propertyName: string,
    fromDecorator = false
  ): void {
    this._addOperation(objectClass, 'excludeProperty', propertyName, null, fromDecorator);
  }

  /**
   * Gets the Exposer data for the named class. This method is used for tests and debug purposes.
   * @param objectClass The contstructor or the name of the class you want to get the Exposer data.
   * @returns Returns a copy of the Exposer data for the named class, or null if the class is not referenced.
   */
  public getExposerData(objectClass: TConstructor | string): IExposedClassData {
    const className = typeof objectClass === 'function' ? objectClass.name : objectClass;
    const classData = this._getClassData(className);
    return classData !== null ? Object.assign({ }, classData) : null;
  }

  //#endregion

  //#region Private methods

  /**
   * Adds an unresolved operation to the list.
   * @param objectClass A constructor method, an instance or the name of the class on which the operation will be performed.
   * @param operationType The type of the operation to perform.
   * @param propertyName The name of the property on which to apply the operation. Null if the operation applies to a class.
   * @param groups The eventual group(s) that the class or the property should be exposed for. Null if the expose operation doesn't target
   * any group or if it's a an exclude operation.
   * @param fromDecorator If true, add the operation at the top of the list, as decorator operations are resolved backwards. Otherwise, add
   * the operation after othe existing ones.
   * @throws {TypeError} Thrown if the given "objectClass" argument is not valid.
   */
  private _addOperation(
    objectClass: TConstructor | string | Record<string, unknown>,
    operationType: TExposerOperation,
    propertyName: string = null,
    groups: string | string[] = null,
    fromDecorator = false
  ): void {
    const operation: IExposerOperation = {
      type: operationType,
      propertyName,
      groups: groups ? asArray(groups) : []
    };

    let className: string = null;
    // If the given value is a constructor method
    if (typeof objectClass === 'function') {
      className = objectClass.name;
    }
    else if (typeof objectClass === 'object') {
      className = objectClass.constructor.name;
    }
    else if (typeof objectClass === 'string') {
      className = objectClass;
    }
    else {
      throw new TypeError('The target object class must be a constructor method, an instance of an object or the name of a class');
    }

    const unresolvedOperationsList = this._unresolvedOperations.get(className);
    // If the target class doesn't have any unresolved operations, create a new list
    if (unresolvedOperationsList === undefined) {
      this._unresolvedOperations.set(className, new Array<IExposerOperation>(operation));
    }
    // Else, if the target class already has unresolved operations
    else {
      // If the operation is added from a decorator, add the operation at the top of the list (since the decorator operations are resolved
      // backwards)
      if (fromDecorator) {
        unresolvedOperationsList.unshift(operation);
      }
      // Else, simply add the operation to the list
      else {
        unresolvedOperationsList.push(operation);
      }
    }
  }

  /**
   * Gets the Exposer data about the named class. If that class has unresolved operations, apply them before.
   * @param className The name of the class you want to get the data.
   * @returns Returns the found class data, otherwise null.
   */
  private _getClassData(className: string): IExposedClassData {
    const unresolvedOperations = this._unresolvedOperations.get(className);
    if (unresolvedOperations !== undefined) {
      this._resolveOperations(className, unresolvedOperations);
      this._unresolvedOperations.delete(className);
    }

    return this._exposerData.get(className) || null;
  }

  /**
   * Resolves the given operations.
   * @param operations The Exposer operations to resolve.
   */
  private _resolveOperations(className: string, operations: IExposerOperation[]) {
    operations.forEach(operation => {
      let classData = this._exposerData.get(className);
      // If the target class is not yet referenced, create its data
      if (classData === undefined) {
        classData = {
          exposed: false,
          // If the operation exposes a class, set its groups, otherwise create an empty groups list
          groups: operation.type === 'exposeClass' ? new Set(operation.groups) : new Set(),
          properties: new Map()
        };
        this._exposerData.set(className, classData);
      }

      // If the operation exposes a class, marks it exposed
      if (operation.type === 'exposeClass') {
        // If the current operation doesn't target a specific group, mark the class eposed for any group
        if (operation.groups.length === 0) {
          classData.groups.clear();
        }
        classData.exposed = true;
        return;
      }

      // If the operation targets a property, get its data
      const propertyData = classData.properties.get(operation.propertyName);

      // If the operation exposes a property
      if (operation.type === 'exposeProperty') {
        // If the property is not referenced yet, create its data
        if (propertyData === undefined) {
          classData.properties.set(operation.propertyName, {
            excluded: false,
            groups: new Set(classData.exposed ? null : operation.groups)
          });
        }
        // Else, if the property is referenced
        else {
          // If the target groups list is empty, mark the property exposed for any group
          if (operation.groups.length === 0) {
            propertyData.excluded = false;
            propertyData.groups.clear();
          }
          // Else, if the target groups list is not empty, add these groups to the property's groups list. Note that for excluded
          // properties, its groups list represents the "excepted groups".
          else {
            if (propertyData.excluded || !classData.exposed) {
              operation.groups.forEach(g => {
                propertyData.groups.add(g);
              });
            }
          }
        }
      }

      // If the operation exludes a property
      else if (operation.type === 'excludeProperty') {
        // If the property is not referenced, create its data
        if (propertyData === undefined) {
          classData.properties.set(operation.propertyName, {
            excluded: true,
            groups: new Set()
          });
        }
        // Else, if the property is referenced, mark it excluded, and clear its groups list
        else {
          propertyData.excluded = true;
          propertyData.groups.clear();
        }
      }
    });
  }

  /**
   * Exposes the given object, by creating a new object that contains only the "exposable" properties of its class.
   * @param className The class name of the object to expose.
   * @param obj The object to expose.
   * @param groups If given, only the properties exposed for the named groups will exist on the ouput object.
   * @Returns Returns a new object that contains the exposed properties, or returns null if the input object is null.
   */
  private _generateExposedObject<TObject>(className: string, obj: TObject, groups: string|string[] = null): Partial<TObject> {
    if (obj === null) { return null; }

    const classData = this._getClassData(className);

    // If the target class is not referenced, returns an empty object
    if (classData === null) { return { }; }

    const exposedObj: Record<string, unknown> = { };
    groups = groups ? asArray(groups) : [];

    // If the class data doesn't have any referenced property
    if (classData.properties.size === 0) {
      // Return an empty object if the class is not even exposed (which shouldn't happend)
      if (!classData.exposed) { return { }; }

      // If the target groups match with the class groups list or if the class is exposed to any group, expose all the object's properties
      if (this._groupMatch(classData.groups, groups, true)) {
        Object.keys(obj).forEach(k => {
          this._addExposedProperty(exposedObj, (obj as Record<string, unknown>), k, groups as string[]);
        });
        return exposedObj as Partial<TObject>;
      }
      // Else, if the target groups don't match with the class groups list, return an empty object
      else {
        return { };
      }
    }
    // Else, if the class has referenced properties, but has a group that doesn't match with target groups, return an empty object
    else if (!this._groupMatch(classData.groups, groups, true)) {
      return { };
    }

    // Get all the object properties, and add the misisng referenced properties to the list
    // These missing properties can be accessors (which are not get using Object.keys())
    const properties = Object.keys(obj);
    classData.properties.forEach((propertyData, prop) => {
      if (!properties.includes(prop)) {
        properties.push(prop);
      }
    });

    // For each property of the given object
    properties.forEach(prop => {
      const propertyData = classData.properties.get(prop);

      // If the current property doesn't exist on the object, cancel
      if ((obj as Record<string, unknown>)[prop] === undefined) { return; }

      // If the current property is not referenced
      if (propertyData === undefined) {
        // If the class is exposed, expose the property
        if (classData.exposed) {
          this._addExposedProperty(exposedObj, (obj as Record<string, unknown>), prop, groups as string[]);
        }
      }
      // Else, if the property is referenced
      else {
        // If the property is excluded
        if (propertyData.excluded) {
          // If the property has "excepted groups"
          if (propertyData.groups.size > 0) {
            // If the target groups match the excepted groups of the property, expose it
            if (this._groupMatch(propertyData.groups, groups as string[])) {
              this._addExposedProperty(exposedObj, (obj as Record<string, unknown>), prop, groups as string[]);
            }
          }
        }
        // Else, if the property is exposed
        else {
          // If the target groups match with the property groups or if the property is exposed to any group, expose it
          if (this._groupMatch(propertyData.groups, groups as string[], true)) {
            this._addExposedProperty(exposedObj, (obj as Record<string, unknown>), prop, groups as string[]);
          }
        }
      }
    });

    return exposedObj as Partial<TObject>;
  }

  /**
   * Used in _generateExposedObject().
   */
  private _addExposedProperty(
    exposedObj: Record<string, unknown>,
    inputObject: Record<string, unknown>,
    propertyName: string,
    groups: string[] = null
  ) {
    // If the property to expose is a valid object
    if (inputObject[propertyName] && typeof inputObject[propertyName] === 'object') {
      // If an Exposer data is set for that object
      if (this._getClassData(inputObject[propertyName].constructor.name) !== null) {
        // Pick only the exposed property of the input object
        exposedObj[propertyName] = this._generateExposedObject(
          inputObject[propertyName].constructor.name,
          inputObject[propertyName],
          groups
        );

        return;
      }
    }

    // In any other case, just set the full object to the exposed property
    exposedObj[propertyName] = inputObject[propertyName];
  }

  /**
   * Checks if one of the data groups match with at least one of the target groups.
   * @param dataGroups The groups list from a class or a property data.
   * @param targetGroups The groups list you want to target.
   * @param validIfNoDataGroups If true, this method will return true if the given data groups set is empty.
   * @returns Returns true if one of the data group matches with the targetGroups, or false if there's no match or if the dataGroups list is
   * empty.
   */
  private _groupMatch(dataGroups: Set<string>, targetGroups: string[], validIfNoDataGroups = false): boolean {
    if (dataGroups.size === 0) {
      return validIfNoDataGroups;
    }

    // For each group from a class or property data
    for (const group of dataGroups) {
      // If at least one of the target groups match with the class or property data groups
      if (targetGroups.includes(group)) {
        return true;
      }
    }

    return false;
  }

  //#endregion

}

//#endregion Exposer API

/**
 * @constant exposerInstance Contains the main Exposer instance.
 */
const exposerInstance = new Exposer();

/**
 * Makes a new object that contains only the exposed properties.
 * @template TObject The type of the input object.
 * @param instance The instance of a class you want to expose.
 * @param groups If given, only the properties exposed for the named groups will be set on the output object.
 * @returns Returns a new object with the exposed properties.
 */
export function expose<TObject = Record<string, unknown>>(instance: TObject, groups?: string | string[]): Partial<TObject>;

/**
 * Makes a new object that contains only the exposed properties.
 * @template TObject The type of the input object.
 * @param className The name of the class of the object to expose.
 * @param obj The object you want to expose.
 * @param groups If given, only the properties exposed for the named groups will be set on the output object.
 * @returns Returns a new object with the exposed properties.
 */
export function expose<TObject = Record<string, unknown>>(
  className: string,
  obj: TObject | Record<string, unknown>,
  groups?: string | string[]
): Partial<TObject>;

/**
 * Makes a new object that contains only the exposed properties.
 * @template TObject The type of the input object.
 * @param objectClass The class or (class constructor method) of the object to expose.
 * @param obj The object to expose. Can be either an instance of the target class, or a simple JavaScript object.
 * @param groups If given, only the properties exposed for the named groups will be set on the output object.
 * @returns Returns a new object with the exposed properties.
 */
export function expose<TObject = Record<string, unknown>>(
  objectClass: TConstructor<TObject>,
  obj: TObject | Record<string, unknown>,
  groups?: string | string[]
): Partial<TObject>;

/**
 * Makes a new object that contains only the exposed properties.
 * @template TObject The type of the input object.
 * @param objectClass An instance of the class you want to expose.
 * @param obj The object to expose. Can be either an instance of the target class, or a simple JavaScript object.
 * @param groups Used only if the first parameter is a class constructor or a class name. If given, only the properties exposed for the
 * named groups will be set on the output object.
 * @returns Returns a new object with the exposed properties.
 * @throws {TypeError} Thrown if the "objectClassOrObj" parameter is neither a string, a constructor method or an instance of a class.
 */
export function expose<TObject = Record<string, unknown>>(
  objectClass: TObject,
  obj: TObject | Record<string, unknown>,
  groups?: string | string[]
): Partial<TObject>;

/**
 * Makes a new object that contains only the exposed properties.
 * @template TObject The type of the input object.
 * @param objectClassOrObj An instance of a class, a class constructor method or the class name of the object to expose.
 * @param objOrGroups If the previous parameter is an object, this should contain the group(s) you want to target. In that case, only the
 * properties exposed for the named groups will be set on the output object. If the previous parameter is a class constructor or a class
 * name, this should contain the object you want to expose (even a instance of the named class, or a "regular" object).
 * @param groups Used only if the first parameter is a class constructor or a class name. If given, only the properties exposed for the
 * named groups will be set on the output object.
 * @returns Returns a new object with the exposed properties.
 * @throws {TypeError} Thrown if the "objectClassOrObj" parameter is neither a string, a constructor method or an instance of a class.
 */
export function expose<TObject = Record<string, unknown>>(
  objectClassOrObj: TConstructor<TObject> | string | TObject,
  objOrGroups?: TObject | Record<string, unknown> | string | string[],
  groups?: string | string[]
): Partial<TObject> {
  return exposerInstance.expose<unknown>(objectClassOrObj, objOrGroups, groups) as Partial<TObject>;
}

/**
 * Marks a class or a property "exposed".
 * @param objectClass A constructor method, an instance or the name of the class on which the operation will be performed.
 * @param propertyName The name of the property on which to apply the operation. Null if the operation applies to a class.
 * @param groups The eventual group(s) that the class or the property should be exposed for. If null given, the property is exposed to any
 * group.
 * @throws {TypeError} Thrown if the given "objectClass" argument is not valid.
 */
export function markExposed(
  objectClass: TConstructor | string | Record <string, unknown>,
  propertyName: string = null,
  groups: string | string[] = null
): void {
  exposerInstance.markExposed(objectClass, propertyName, groups, false);
}

/**
 * Marks a property "excluded", so it won't be exposed to any group.
 * @param objectClass A constructor method, an instance or the name of the class on which the operation will be performed.
 * @param propertyName The name of the property on which to apply the operation.
 * @throws {TypeError} Thrown if the given "objectClass" argument is not valid.
 */
export function markExcluded(
  objectClass: TConstructor | string | Record <string, unknown>,
  propertyName: string
): void {
  exposerInstance.markExcluded(objectClass, propertyName, false);
}

//#endregion

export { exposerInstance };
export default expose;
