import { TConstructor } from './types';
import { exposerInstance } from './exposer';

/**
 * Marks a class or a property as exposed.
 * @param groups The eventual group(s) you want that class or property to be exposed for. If null or empty array given, the class or the
 * property is exposed to every groups.
 */
export function Expose(groups: string|string[] = null) {
  return (object: unknown | TConstructor, propertyName: string = null): void => {
    exposerInstance.markExposed(object as TConstructor | Record<string, unknown>, propertyName, groups, true);
  };
}

/**
 * Marks the property as excluded. This is useful for exposed classes, when you want to "expose all but...". You can also use it in
 * addition of @Expose() on a property, so you can exclude a property but for some groups.
 */
export function Exclude() {
  return (prototype: unknown, propertyName: string): void => {
    exposerInstance.markExcluded(prototype as Record<string, unknown>, propertyName, true);
  };
}
