/**
 * @type Defines an Exposer operation.
 */
export type TExposerOperation = 'exposeClass' | 'exposeProperty' | 'excludeProperty';

/**
 * @type Defines a constructor method.
 */
export type TConstructor<TObject = Record<string, unknown>> = new () => TObject;
