import { addTest } from './tests';
import { expose, Expose, Exclude } from '..';

@Expose()
class Groups {
  // Should be exposed only for group "private"
  @Exclude() @Expose('private')
  private id = 4;

  // Should be exposed to any group
  public username = 'John Doe';

  // Should be exposed only for group "private"
  @Exclude() @Expose('private')
  public password = 'ThisIsASecret';

  // Should be exposed to any group (the enntire class is exposed)
  @Expose('private')
  public email = 'test@example.com';

  // Should be excluded to any group
  @Exclude()
  private _cached = 'THIS VALUE SHOULD NEVER APPEAR';
}

const data = new Groups();

addTest('Groups without group', Groups, expose(data), [ 'username', 'email' ]);
addTest('Groups with "private" group', Groups, expose(data, 'private'), [ 'id', 'password', 'username', 'email' ]);
addTest('Groups with "unknown" group', Groups, expose(data, 'unknown'), [ 'username', 'email' ]);
