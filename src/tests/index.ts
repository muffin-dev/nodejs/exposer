// Import test scripts
import './double-expose';
import './groups';
import './multiple-excludes';
import './class-group';
import './nested-objects';
import './null-data';
import './accessors';

// Import & execute test engine
import { runTests } from './tests';
runTests(true);
