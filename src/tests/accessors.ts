import { expose, Expose, Exclude } from '../lib';
import { addTest } from './tests';

@Expose()
class Accessors {
  public a = 'A';

  @Exclude()
  private _b = 'B';

  @Exclude()
  private _c = 'C';

  @Expose()
  public get b() { return this._b; }

  public set b(val: string) { this._b = val; }

  @Exclude() @Expose('test')
  public get c() { return this._c; }

  public set c(val: string) { this._c = val; }
}

const data = new Accessors();
addTest('Accessors', Accessors, expose(data), [ 'a', 'b' ]);
addTest('Accessors with test', Accessors, expose(data, 'test'), [ 'a', 'b', 'c' ]);
addTest('Accessors with invalid group', Accessors, expose(data, 'unknown'), [ 'a', 'b' ]);
