import { expose, Expose, Exclude } from '../lib';
import { addTest } from './tests';

@Expose('group1')
@Expose()
class DoubleExpose {
  public a = 'A';
  public b = 'B';
  @Exclude()
  private c = 'C';
}

const test = new DoubleExpose();

addTest('Double @Expose', DoubleExpose, expose(test), [ 'a', 'b' ]);
addTest('Double @Expose with group1', DoubleExpose, expose(test, 'group1'), [ 'a', 'b' ]);
addTest('Double @Expose with invalid group', DoubleExpose, expose(test, 'unknown'), [ 'a', 'b' ]);
