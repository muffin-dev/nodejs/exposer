import { addTest } from './tests';
import { expose } from '..';

class NullData {
  public a = 'a';
  public b = 'b';
  public v = 'v';
}

const data = new NullData();
addTest('Null data without group', NullData, expose(data), [ ]);
addTest('Groups with "unknown" group', NullData, expose(data, 'unknown'), [ ]);
