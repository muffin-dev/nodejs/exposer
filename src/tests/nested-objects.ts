/* eslint-disable max-classes-per-file */

import { expose, Expose, Exclude } from '../lib';
import { addTest } from './tests';

@Expose()
class NestedObjectsUserProfile {
  public city = '';

  @Exclude() @Expose('private')
  public address = '';

  @Exclude() @Expose('private')
  public zipcode = '';
}

@Expose('private')
class NestedObjectsLocation {
  public lat = '';
  public lon = '';
}

@Expose()
class NestedObjectsUser {
  public username = '';
  public email = '';

  @Exclude() @Expose('private')
  public password = '';

  public profile = new NestedObjectsUserProfile();

  public location = new NestedObjectsLocation();
}

const user = new NestedObjectsUser();
addTest('Nested objects without group', NestedObjectsUser, expose(user), [ 'username', 'email', 'profile', 'profile.city', 'location' ]);
addTest('Nested objects with "private" group', NestedObjectsUser, expose(user, 'private'), [ 'username', 'password', 'email', 'profile', 'profile.city', 'profile.address', 'profile.zipcode', 'location', 'location.lat', 'location.lon' ]);
addTest('Nested objects with "unknown" group', NestedObjectsUser, expose(user, 'unknown'), [ 'username', 'email', 'profile', 'profile.city', 'location' ]);
