import { expose, Expose, Exclude } from '../lib';
import { addTest } from './tests';

// Should finally expose the class to any group
@Expose('test')
@Expose()
class MultipleExcludes {
  // Should let the property exposed to any group: the class is exposed
  @Expose('test')
  public d = 'D';

  // This should finally cause the property to be ignored to any group
  @Exclude() @Expose('test') @Exclude()
  public e = 'E';

  // This should finally cause the property to be exposed to any group
  @Exclude() @Expose('test') @Expose()
  private f = 'F';

  // This should finally cause the property to be only for "test" group
  @Exclude() @Expose('test')
  private g = 'G';
}

const test = new MultipleExcludes();

addTest('Multiple excludes', MultipleExcludes, expose(test), [ 'd', 'f' ]);
addTest('Multiple excludes using groups', MultipleExcludes, expose(test, 'test'), [ 'd', 'f', 'g' ]);
