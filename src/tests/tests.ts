import { inspect } from 'util';
import { exposerInstance } from '../lib/exposer';
import { findPropertyValue } from '@muffin-dev/js-helpers';

/**
 * @interface ITestData Informations about a test to execute.
 */
interface ITestData {
  /**
   * @property The name of the test to perform.
   */
  name: string;

  /**
   * @property The class name of the tested object.
   */
  className: string;

  /**
   * @property The tested object (with has been processed using Exposer).
   */
  obj: Record<string, unknown>;

  /**
   * @property The expected properties that should appear on the tested object.
   */
  expectedProperties: string[];
}

const TESTS = new Array<ITestData>();

/**
 * Adds a test to the list.
 * @param name The name of the test (for logging errors only).
 * @param objectClass The class constructor or the class name of the tested object.
 * @param obj The object to test.
 * @param exposedProperties The list of the properties that should be accessible on the tested object.
 */
export function addTest<TObject = Record<string, unknown>>(
  name: string,
  objectClass: (new () => TObject) | string,
  obj: Record<string, unknown>,
  expectedProperties: string[]
): void {
  TESTS.push({ name, className: typeof objectClass === 'function' ? objectClass.name : objectClass, obj, expectedProperties });
}

/**
 * Run all tests by checking if the exposed objects have the appropriate properties.
 * @param logData If true, logs the Exposer data of the class used for the errored tests.
 */
export function runTests(logData = false): void {
  const errors = new Array<string>();
  const exposerData = new Map<string, unknown>();

  TESTS.forEach(data => {
    // For each expected property of the list
    data.expectedProperties.forEach(prop => {
      // If the expected property doesn't exist on the exposed object
      const val = findPropertyValue(data.obj, prop, null);
      if (val === null || val === undefined) {
        errors.push(`Test "${data.name}" failed: the property "${prop}" should appear on the exposed object`);
        exposerData.set(data.className, exposerInstance.getExposerData(data.className));
      }
    });

    // For each input object property
    Object.keys(data.obj).forEach(k => {
      // @todo: Tests does not apply to the nested properties of the input object for now

      // If the object property shouldn't be exposed
      if (!data.expectedProperties.includes(k)) {
        errors.push(`Test "${data.name}" failed: the property "${k}" should not be exposed`);
        exposerData.set(data.className, exposerInstance.getExposerData(data.className));
      }
    });
  });

  if (errors.length === 0) {
    console.log('All tests passed successfully!');
  }
  else {
    console.error('Tests failed:');
    errors.forEach(err => {
      console.warn('\t' + err);
    });

    if (logData && exposerData.size > 0) {
      console.log('\n');
      console.log('Exposer data for errored tests:');
      exposerData.forEach((data, name) => {
        console.warn(`\tData for class "${name}"`, inspect(data, { colors: true, depth: 3, }));
      });
    }
  }
}
