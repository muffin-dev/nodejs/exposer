import { addTest } from './tests';
import { expose, Expose, Exclude } from '..';

@Expose('test')
class ClassGroup {
  // Should expose the property only for group "test"
  public a = 'A';

  // Should expose the property only for group "test" anyway
  @Expose()
  public b = 'B';

  // Should expose the property only for group "test" anyway
  @Expose('unkown')
  public c = 'C';

  // Should exclude the property
  @Exclude()
  public d = 'D';

  // Should exclude the property for any group anyway
  @Exclude() @Expose('unkown')
  public e = 'E';
}

const data = new ClassGroup();

addTest('Class group without group', ClassGroup, expose(data), [ ]);
addTest('Class group with "test" group', ClassGroup, expose(data, 'test'), [ 'a', 'b', 'c' ]);
addTest('Class group with "unknown" group', ClassGroup, expose(data, 'unknown'), [ ]);
